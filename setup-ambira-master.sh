设置从本master节点到其他slave节点的免密登陆

ssh-keygen

ssh-keygen -f "/root/.ssh/known_hosts" -R node1.puppy.org
ssh-keygen -f "/root/.ssh/known_hosts" -R node3.puppy.org
ssh-keygen -f "/root/.ssh/known_hosts" -R node1
ssh-keygen -f "/root/.ssh/known_hosts" -R node3
ssh-keygen -f "/root/.ssh/known_hosts" -R 166.111.x.x
ssh-keygen -f "/root/.ssh/known_hosts" -R 166.111.x.x


ssh-copy-id root@node1.puppy.org
ssh root@node1.puppy.org
### input "yes"

ssh-copy-id root@node3.puppy.org
ssh root@node1.puppy.org
### input "yes"



# Install Ambari Server (on master node)
wget -nv http://public-repo-1.hortonworks.com/ambari/centos7/2.x/updates/2.7.3.0/ambari.repo -O /etc/yum.repos.d/ambari.repo
yum repolist
yum install -y ambari-server


# mysql
yum install mysql-connector-java



# Local Repository
## http server (apache httpd)
# yum install -y httpd
# mkdir -p /var/www/html/
# systemctl start httpd

## Setting up a Local Repository with Temporary Internet Access
# yum install yum-utils createrepo

# OS=centos7

# cd /var/www/html
# mkdir -p ambari/$OS
# cd ambari/$OS
# reposync -r Updates-Ambari-2.7.3.0

# cd /var/www/html
# mkdir -p hdp/$OS
# cd hdp/$OS
# reposync -r HDP-3.1.0.0
# reposync -r HDP-UTILS-1.1.0.22

# createrepo /var/www/html/ambari/$OS/Updates-Ambari-2.7.3.0
# createrepo /var/www/html/hdp/$OS/HDP-3.1.0.0
# createrepo /var/www/html/hdp/$OS/HDP-UTILS-1.1.0.22

# unset OS

