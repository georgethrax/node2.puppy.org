# node2.puppy.org
---

# 重装系统
以node2为例，安装CentOS-7.5
## 准备安装光盘
- 下载CentOS-7.5安装镜像 https://mirrors.tuna.tsinghua.edu.cn/centos/7.6.1810/isos/x86_64/CentOS-7-x86_64-DVD-1804.iso
- 将iso镜像刻录一光盘
- 在node2机器上从光盘启动

## 系统安装设置
### 设置安装类型
- 英文
- 带GUI的服务器安装模式
- Java, KDE, 开发工具, 系统管理工具

### 设置网络
- 启动时自动连接: yes
- 手动设置静态ip地址: manual(static)
- ip: 166.111.x.x
- 子网掩码netmask: 255.255.255.224
- 网关gateway: 166.111.72.193
- DNS:166.111.8.28 166.111.8.29

### 设置域名
必须是完全限定域名FQDN的形式。
这里设定为,
```
node2.puppy.org
```

### 设置时区
```
Asia/Shanghai
```
TUNA有一台ntp服务器`ntp.tuna.tsinghua.edu.cn`，但目前尚未设置

### 设置磁盘分区
目前采用ext4文件系统，为根目录挂载点`/`设置了640GB的空间，为家目录挂载点`/home`设置了3.0TB的空间

## 开始安装
这期间可以设置root密码，新建一个非root的管理员账号

## 结束安装
- 重启
- 屏幕点选接收许可协议
- 安装完毕。

# 新系统设置和安装软件
如非特殊声明，均以具有sudo权限的非root账户登录。

## yum换源清华镜像
这部分整理为了一个脚本`setup-tuna-repo.sh`，然后执行
```
git clone https://gitlab.com/georgethrax/node2.puppy.org.git
cd node2.puppy.org
sudo sh setup-tuna-repo.sh
```

所换的yum源：
- CentOS源
- EPEL源
- docker-ce源

## 安装辅助软件
```
sudo sh install-useful-softwares.sh
```


# 安装Ambira
系统语言修改为en_US
## 计划使用的版本
详见 https://supportmatrix.hortonworks.com/

- CentOS-7.5.1804
- Ambari-2.7.3
- HDP-3.1.0
- PostgreSQL-10.2/9.6
- Firefox-52/51, Chrome-57.0.2/56.0.2, Safari-10.0.3/10.0.1
- OracleJDK8(JDK1.8.0_77)
- Processor-x86_64

## 在master节点执行本机设置脚本
不涉及对slave节点的操纵
```
sudo sh setup-ambira-common.sh
```

## 在master节点执行安装脚本
```
sudo setup-ambira-master.sh
```

## 开始配置Ambari Server
```
ambari-server setup
```
设置项目;
- SELinux 'permissive': `y`
- Customize user account for ambari-server daemon: `n`
- JDK: `1` (Oracle JDK 1.8 + JCE 8)
- Do you accept the Oracle Binary Code License Agreement: `y`
- install GPL Licensed LZO packages: `y`
- Enter advanced database configuration: `n`

Ambari Server 'setup' completed successfully.

```
ambari-server setup --jdbc-db=mysql --jdbc-driver=/usr/share/java/mysql-connector-java.jar
ls -l /var/lib/ambari-server/resources/mysql-connector-java.jar
```

## 启动Ambari Server
注意：Ambari Server监听`8080`端口
```
ambari-server start
ambari-server status
```


## 在slave节点执行本机设置脚本
假定在安装操作系统时，slave节点自己的全称域名FQDN已经设置好
形如node1.puppy.org
### 手动操作步骤
- git clone 本工程
- 将 ./setup-ambira-common/hosts文件准备好
- vi /etc/sysconfig/network https://docs.hortonworks.com/HDPDocuments/Ambari-2.7.3.0/bk_ambari-installation/content/edit_the_network_configuration_file.html

### 脚本操作步骤
其余步骤可以手动操作
```
sudo sh setup-tuna-repo.sh
sudo sh setup-ambira-common.sh
sudo sh install-useful-softwares.sh
```



## 通过Ambari Server部署HDP
### 通过浏览器登陆Ambari Server
浏览器访问 http://node2.puppy.org:8080
默认密码admin, admin
修改admin账号的密码
### 点击 Launch Install Wizard
### Name Your Cluster 
`ClusterPuppy`
### Select Version
- HDP-3.1.0.0
- Public Repository
- redhat7(centos7, 删除其他的操作系统选项)
- 不勾选"Skip Repository Base URL validation (Advanced)

### Install Options
- Target Hosts: `node[1-3].puppy.org`
- Host Registration Information: `Provide your SSH Private Key to automatically register hosts`
- ssh private key: 将/root/id_rsa的内容复制进编辑框里
- SSH User Account: `root`默认
- SSH Port Number: `22`默认

### ​Confirm Hosts
按图形界面操作。
单击`Status`一列下的文字可以看到日志。
一切正常，仅有1个warning：`chronyd Not running`，但`chronyd`是ntp的替代品，不用安装。

### Choose File System
HDFS-3.1.1

### Choose Services
- YARN + MapReduce2	3.1.1 Apache Hadoop NextGen MapReduce (YARN)
- Tez	0.9.1	Tez is the next generation Hadoop Query Processing framework written on top of YARN.
- Hive	3.1.0 Data warehouse system for ad-hoc queries & analysis of large datasets and table & storage management service
- ZooKeeper	3.4.6 Centralized service which provides highly reliable distributed coordination
- Infra Solr	0.1.0 Core shared service used by Ambari managed components.
- Ambari Metrics	0.1.0	A system for metrics collection that provides storage and retrieval capability for metrics collected from the cluster
- SmartSense	1.5.1.2.7.3.0-139	SmartSense - Hortonworks SmartSense Tool (HST) helps quickly gather configuration, metrics, logs from common HDP services that aids to quickly troubleshoot support cases and receive cluster-specific recommendations.
- Spark2	2.3.2	Apache Spark 2.3 is a fast and general engine for large-scale data processing.

### Assign Masters
#### node1.puppy.org (62.5 GB, 40 cores)
NameNode ResourceManager Timeline Service V2.0 Reader YARN Registry DNS ZooKeeper Server Infra Solr Instance Grafana Activity Explorer HST Server Activity Analyzer Spark2 History Server
#### node2.puppy.org (94.0 GB, 40 cores)
SNameNode Timeline Service V1.5 History Server Hive Metastore HiveServer2 ZooKeeper Server
#### node3.puppy.org (94.0 GB, 40 cores)
ZooKeeper Server Metrics Collector

#### Assign Slaves and Clients
- node1.puppy.org: Client
- node2.puppy.org: Client
- node3.puppy.org: DataNode, NodeManager, Client
"Client" will install HDFS Client, YARN Client, MapReduce2 Client, Tez Client, Hive Client, Sqoop Client, Oozie Client, ZooKeeper Client, Infra Solr Client and Spark2 Client.

#### credentials 
services        |Username   |Password   |
| ------        | ------    | ------    |
Grafana Admin   | `admin`     | ***     |
Hive Database   | `hive`      | ***     |
Oozie Database  | `oozie`     | ***     |
Activity Explorer's Admin   | N/A   | ***   |

#### databases 
##### HIVE
New MySQL

#### Review
生成了`blueprint`文件夹

```
Admin Name : admin

Cluster Name : PuppyCluster

Total Hosts : 3 (3 new)

Repositories:

redhat7 (HDP-3.1): 
http://public-repo-1.hortonworks.com/HDP/centos7/3.x/updates/3.1.0.0
redhat7 (HDP-3.1-GPL): 
http://public-repo-1.hortonworks.com/HDP-GPL/centos7/3.x/updates/3.1.0.0
redhat7 (HDP-UTILS-1.1.0.22): 
http://public-repo-1.hortonworks.com/HDP-UTILS-1.1.0.22/repos/centos7
Services:

HDFS
DataNode : 1 host
NameNode : node1.puppy.org
NFSGateway : 0 host
SNameNode : node2.puppy.org
YARN + MapReduce2
Timeline Service V1.5 : node2.puppy.org
NodeManager : 1 host
ResourceManager : node1.puppy.org
Timeline Service V2.0 Reader : node1.puppy.org
Registry DNS : node1.puppy.org
Tez
Clients : 3 hosts
Hive
Metastore : node2.puppy.org
HiveServer2 : node2.puppy.org
Database : New MySQL Database
ZooKeeper
Server : 3 hosts
Infra Solr
Infra Solr Instance : node1.puppy.org
Ambari Metrics
Metrics Collector : node3.puppy.org
Grafana : node1.puppy.org
SmartSense
Activity Analyzer : node1.puppy.org
Activity Explorer : node1.puppy.org
HST Server : node1.puppy.org
Spark2
Livy for Spark2 Server : 0 host
History Server : node1.puppy.org
Thrift Server : 0 host
```

---
# 其他
## git代理
git config --global http.proxy "http://$ip:$port"
git config --global https.proxy "http://$ip:$port"
