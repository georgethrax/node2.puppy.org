#!/usr/bin/bash
# setup-tuna-repo.sh
set -v 

LOCAL_REPO_DIR="setup-tuna-repo"

# Download .repo files
# echo -e "\033[31m[INFO]Downloading .repo files\033[0m"
# wget -O $(LOCAL_REPO_DIR)/CentOS-Base.repo https://gitlab.com/georgethrax/node2.puppy.org/raw/master/setup-tuna-repo/CentOS-Base.repo
# wget -O $(LOCAL_REPO_DIR)/epel.repo https://gitlab.com/georgethrax/node2.puppy.org/raw/master/setup-tuna-repo/epel.repo
# wget -O $(LOCAL_REPO_DIR)/docker-ce.repo https://gitlab.com/georgethrax/node2.puppy.org/raw/master/setup-tuna-repo/docker-ce.repo
# # wget -O $(LOCAL_REPO_DIR)/docker-ce.repo https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
# # sed -i 's+download.docker.com+mirrors.tuna.tsinghua.edu.cn/docker-ce+' $(LOCAL_REPO_DIR)/docker-ce.repo
# echo -e "\033[31m[INFO]Downloading .repo files Done.\033[0m"


# CentOS-Base.repo
echo -e "\033[31m[INFO]editing /etc/yum.repos.d/CentOS-Base.repo\033[0m"
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
cp $LOCAL_REPO_DIR/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo
yum makecache
echo -e "\033[31m[INFO]CentOS-Base.repo Done.\033[0m"

# epel.repo
yum install -y epel-release
echo -e "\033[31m[INFO]editing /etc/yum.repos.d/epel.repo\033[0m"
mv /etc/yum.repos.d/epel.repo /etc/yum.repos.d/epel.repo.bak
cp $LOCAL_REPO_DIR/epel.repo /etc/yum.repos.d/epel.repo
yum makecache
echo -e "\033[31m[INFO]epel.repo Done.\033[0m"

#docker-ce
echo -e "\033[31m[INFO]editing /etc/yum.repos.d/docker-ce.repo\033[0m"
cp $LOCAL_REPO_DIR/docker-ce.repo /etc/yum.repos.d/docker-ce.repo
yum makecache fast
echo -e "\033[31m[INFO]docker-ce.repo Done.\033[0m"


# yum remove -y docker docker-common docker-selinux docker-engine
# yum install -y yum-utils device-mapper-persistent-data lvm2
# yum install -y docker-ce
# if [ $? -ne 0 ]; then
#     echo -e "\033[31m[INFO]docker-ce installed.\033[0m"
# else
#     exit
# fi
