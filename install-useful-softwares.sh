# install-useful-softwares.sh
set -v

yum install -y tigervnc-server tigervnc 

for (( i_port = 5901; i_port < 5911; i_port ++ ));do
    iptables -I INPUT -p tcp --dport $i_port -j ACCEPT
done
