#setup-ambira-common.sh
set -v

# Software Requirements
yum install -y ntp scp curl unzip tar openssl


# 最大打开文件
ulimit -n 10000

# ssh-keygen
ssh-keygen

# ntp
yum install -y ntp 
mv /etc/ntp.conf /etc/ntp.conf.bak
cp setup-ambira-master/ntp.conf /etc/ntp.conf
systemctl enable ntpd

# hosts
#sudo sh -c "cat setup-ambira-common/hosts >> /etc/hosts"
cat setup-ambira-common/hosts >> /etc/hosts

# vi /etc/sysconfig/network

# 关闭防火墙
systemctl disable firewalld
service firewalld stop

# SELinux, umask
setenforce 0
echo -e "\033[31m[INFO]忽略了PackageKit问题\033[0m"
echo "https://docs.hortonworks.com/HDPDocuments/Ambari-2.7.3.0/bk_ambari-installation/content/disable_selinux_and_packagekit_and_check_the_umask_value.html"
umask 0022

# 数据库
echo -e "\033[31m[INFO]跳过了数据库配置\033[0m"
echo "https://docs.hortonworks.com/HDPDocuments/Ambari-2.7.3.0/bk_ambari-installation/content/configuring_mysql_for_ranger.html"


